export class Audio {
    id: number;
    name: string;
    // tslint:disable-next-line:variable-name
    id_message: number;
    // tslint:disable-next-line:variable-name
    id_status: number;
    // tslint:disable-next-line:variable-name
    created_at: string;
    // tslint:disable-next-line:variable-name
    updated_at: string;
}
