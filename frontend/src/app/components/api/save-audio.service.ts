import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HeaderService } from 'src/app/components/api/header.service';
import { Observable } from 'rxjs';
import { Save } from 'src/app/save';

@Injectable({
  providedIn: 'root'
})
export class SaveAudioService {

  private urlSaveAudio: string;

  constructor(private http: HttpClient, private headerService: HeaderService) {

  }

  saveAudio(save: Save): any {
    this.urlSaveAudio = this.headerService.url + '/save';
    return this.http.post(this.urlSaveAudio, save, this.headerService.getHeader());
  }
}
