import { TestBed } from '@angular/core/testing';

import { SaveAudioService } from './save-audio.service';

describe('SaveAudioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaveAudioService = TestBed.get(SaveAudioService);
    expect(service).toBeTruthy();
  });
});
