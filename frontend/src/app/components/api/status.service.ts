import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HeaderService } from 'src/app/components/api/header.service';
import { Observable } from 'rxjs';
import { Status } from 'src/app/status';

@Injectable({
  providedIn: 'root'
})
export class StatusService {

  private urlStatus: string;

  constructor(private http: HttpClient, private headerService: HeaderService) {
  }

  createStatus(status: Status): any {
    this.urlStatus = this.headerService.url + '/status';
    return this.http.post(this.urlStatus, status, this.headerService.getHeader());
  }

  getStatus(idStatus: number) {
    this.urlStatus = this.headerService.url + '/status';
    this.urlStatus += '/' + idStatus;
    return this.http.get(this.urlStatus, this.headerService.getHeader())
  }
}
