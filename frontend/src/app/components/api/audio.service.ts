import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HeaderService } from 'src/app/components/api/header.service';
import { Observable } from 'rxjs';
import { Audio } from 'src/app/audio';

@Injectable({
  providedIn: 'root'
})
export class AudioService {

  private urlAudio: string;

  constructor(private http: HttpClient, private headerService: HeaderService) {
  }

  createAudio(audio: Audio): any {
    this.urlAudio = this.headerService.url + '/audio';
    return this.http.post(this.urlAudio, audio, this.headerService.getHeader());
  }

  getAllAudios() {
    this.urlAudio = this.headerService.url + '/audio';
    return this.http.get(this.urlAudio, this.headerService.getHeader());
  }

  getAudio(idAudio: number) {
    this.urlAudio = this.headerService.url + '/audio';
    this.urlAudio += '/' + idAudio;
    return this.http.get(this.urlAudio, this.headerService.getHeader());
  }

  updateAudio(audio: Audio, idAudio: number) {
    this.urlAudio = this.headerService.url + '/audio';
    // tslint:disable-next-line:no-unused-expression
    this.urlAudio + idAudio;
    return this.http.put(this.urlAudio, audio, this.headerService.getHeader());
  }
}
