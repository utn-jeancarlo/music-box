import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HeaderService } from 'src/app/components/api/header.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DownloadService {

  private urlDownload: string;

  constructor(private http: HttpClient, private headerService: HeaderService) {

  }

  getDownloadAudio(audioName: string) {
    this.urlDownload = this.headerService.url + '/download';
    this.urlDownload += '/' + audioName;
    return this.http.get(this.urlDownload, this.headerService.getHeader());
  }
}
