import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  public url = 'http://localhost:80/api';

  constructor() {

  }

  getHeader() {
    const header = new HttpHeaders({
      // tslint:disable-next-line:object-literal-key-quotes
      'Accept': 'text/html, application/xhtml+xml, */*',
      'Content-Type': 'application/json',
    });
    return {headers: header};  }

}
