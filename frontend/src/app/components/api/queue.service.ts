import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HeaderService } from 'src/app/components/api/header.service';
import { Observable } from 'rxjs';
import { Queue } from 'src/app/queue';

@Injectable({
  providedIn: 'root'
})
export class QueueService {

  private urlQueue: string;

  constructor(private http: HttpClient, private headerService: HeaderService) {

  }

  createQueue(queue: Queue): any {
    this.urlQueue = this.headerService.url + '/queue';
    return this.http.post(this.urlQueue, queue, this.headerService.getHeader());
  }

  getQueue(idQueue: number) {
    this.urlQueue = this.headerService.url + '/queue';
    this.urlQueue += '/' + idQueue;
    return this.http.get(this.urlQueue, this.headerService.getHeader());
  }
}
