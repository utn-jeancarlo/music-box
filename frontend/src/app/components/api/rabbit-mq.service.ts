import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HeaderService } from 'src/app/components/api/header.service';
import { Observable } from 'rxjs';
import { RabbitMQ } from '../../rabbit-mq';

@Injectable({
  providedIn: 'root'
})
export class RabbitMQService {

  private urlRabbitMQ: string;

  constructor(private http: HttpClient, private headerService: HeaderService) {

  }

  createRabbitMQ(rabbitMQ: RabbitMQ): any {
    this.urlRabbitMQ = this.headerService.url + '/rabbitClient';
    return this.http.post(this.urlRabbitMQ, rabbitMQ, this.headerService.getHeader());
  }
}
