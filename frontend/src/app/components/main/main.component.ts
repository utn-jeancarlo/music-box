import { Component, OnInit } from '@angular/core';
import { Status } from 'src/app/status';
import { Audio } from 'src/app/audio';
import { Queue } from 'src/app/queue';
import { StatusService } from '../api/status.service';
import { QueueService } from '../api/queue.service';
import { AudioService } from '../api/audio.service';
import { SaveAudioService } from '../api/save-audio.service';
import { Save } from 'src/app/save';


import Swal from 'sweetalert2';
import { RabbitMQService } from '../api/rabbit-mq.service';
import { RabbitMQ } from '../../rabbit-mq';
import { DownloadService } from '../api/download.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  public status: Status;
  public audio: Audio;
  public queue: Queue;
  public save: Save;

  public formatSelected = '';
  public fileAdded: File = null;
  public currentAudio = '';
  public idQueue = 0;
  public audioName = '';

  public audioBase64: string;

  public audiosList: Audio[];
  public rabbitList: RabbitMQ[];

  constructor(private statusService: StatusService,
              private queueService: QueueService,
              private audioService: AudioService,
              private saveAudioService: SaveAudioService,
              private downloadAudioService: DownloadService,
              private rabbitMQService: RabbitMQService) {
                this.status = new Status();
                this.audio = new Audio();
                this.queue = new Queue();
                this.audiosList = [];
                this.rabbitList = [];
    }

  ngOnInit() {
    this.getStatus();
    this.getAudio();
    this.getQueue();
    // this.getAllAudios();
  }

  resetInfo() {
    this.formatSelected = '';
    this.fileAdded = null;
    this.currentAudio = '';
    this.idQueue = 0;
    this.audioName = '';
  }

  setAudioList(list: any) {
    list.name = this.audioName + '.' + this.formatSelected;
    this.audiosList.push(list);
  }

  getAudioName() {
    // Remove the name
    // this.audioName = this.currentAudio.slice((this.currentAudio.lastIndexOf(".") - 1 >>> 0) + 2);

    // Remove the extension
    this.audioName = this.currentAudio.substring(0, this.currentAudio.lastIndexOf('.'));
    console.log(this.audioName);
  }

  getStatus() {
    this.statusService.getStatus(1).toPromise()
    .then((res) => {
      this.status = res[0];
      console.log(this.status);
    })
    .catch(err => {
      console.log(JSON.stringify(err));
    });
  }

  getAudio() {
    this.audioService.getAudio(4).toPromise()
    .then((res) => {
      this.audio = res[0];
      console.log(this.audio);
    })
    .catch(err => {
      console.log(JSON.stringify(err));
    });
  }

  getAllAudios() {
    this.audioService.getAllAudios().toPromise()
    .then((res) => {
      this.setAudioList(res);
    })
    .catch(err => {
      console.log(JSON.stringify(err));
    });

    console.log('esta es la lista de audios');

  }

  getQueue() {
    this.queueService.getQueue(1).toPromise()
    .then((res) => {
      this.queue = res[0];
      console.log(this.queue);
    })
    .catch(err => {
      console.log(JSON.stringify(err));
    });
  }

  showInfo() {
    Swal.fire(
      'What are the compatible formats?',
      'MP3, AIF, FLAC, OGG, WAV',
      'question'
    );
  }

// aqui se puede obtener el nombre con la extencion
  fileChanged(e) {
    console.log('ya seleccione el audio');
    console.log(e.target.files[0]);
    // tslint:disable-next-line:no-angle-bracket-type-assertion
    this.fileAdded = <File> e.target.files[0];
    this.currentAudio = e.target.files[0].name;
    this.getAudioName();

    const reader = new FileReader();
    if (e.target.files && e.target.files.length > 0) {
      const file = e.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {

        let readerResult: string;
        readerResult = reader.result.toString();
        // tslint:disable-next-line:no-unused-expression
        this.audioBase64 = readerResult.split(',')[1];

        //  alert(reader.result.split(',')[1]);
        //  console.log(reader.result.split(',')[1]);
        //  this.fileName = file.name + ' ' + file.type;
        //  this.filePreview = 'data:image/png' + ';base64,' + reader.result.split(',')[1];
      };
    }
}

// send the audio to the api
  uploadAudio() {
    this.save = new Save();
    this.save.name = this.currentAudio;
    this.save.file = this.audioBase64;
    this.save.type = this.fileAdded.type;

    this.saveAudioService.saveAudio(this.save).toPromise()
    .then((res) => {
      console.log('Entre al save del api');
      console.log(res);
      this.resetInfo();
  })
  .catch(err => {
    console.log(JSON.stringify(err));
  });
}

  convertAudio() {
    if (!this.formatSelected || !this.fileAdded) {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'You must select an Audio and format!',
      });
    } else {
      this.createQueue();
    }
  }

  createAudio(audio: Audio) {
    this.audioService.createAudio(audio).toPromise()
    .then((res) => {
      this.audio = res[0];
      console.log(this.audio);
      this.uploadAudio();
      this.setAudioList(res);

      const rabbitMQ = new RabbitMQ();
      rabbitMQ.id = this.queue.id;
      rabbitMQ.from = this.queue.from;
      rabbitMQ.to = this.queue.to;
      rabbitMQ.name = this.audioName + '.' + this.formatSelected;

      this.createRabbitMQ(rabbitMQ);

    })
    .catch(err => {
      console.log(JSON.stringify(err));
    });
  }

  createQueue() {

    // In this part is created the queue message
    this.queue = new Queue();
    this.queue.from = '/audios/' + this.currentAudio;
    this.queue.to = '/audios/audiosCompleted/' + this.audioName + '.' + this.formatSelected;

    this.queueService.createQueue(this.queue).toPromise()
    .then((res) => {
      this.queue = res;
      // In this part is created the audio
      this.audio = new Audio();
      this.audio.name = this.currentAudio;
      // tslint:disable-next-line:no-string-literal
      this.audio.id_message = res['id'];
      this.audio.id_status = 1;

      this.createAudio(this.audio);

      console.log('este es el nuevo queue');
      console.log(this.queue);

    })
    .catch(err => {
      console.log(JSON.stringify(err));
    });
  }

  createRabbitMQ(rabbitMQ: RabbitMQ) {
    this.rabbitMQService.createRabbitMQ(rabbitMQ).toPromise()
    .then((res) => {
        console.log(res);
        // tslint:disable-next-line:no-string-literal
        this.updateAudioList(res['id'], res['status']);
        this.rabbitList.push(rabbitMQ);
    })
    .catch(err => {
      console.log(JSON.stringify(err));
    });
  }

  private updateAudioList(audioID: any, newStatus: any) {
    console.log('audio id ', audioID);
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < this.audiosList.length; index++) {
            if (this.audiosList[index].id_message === audioID) {
              this.audiosList[index].id_status = newStatus;
              console.log('new list ', this.audiosList);
              return ;
            }
      }
  }

  getAudioDownloaded(audioID: any) {
    // tslint:disable-next-line:prefer-for-of
    for (let index = 0; index < this.rabbitList.length; index++) {
      if (audioID === this.rabbitList[index].id) {
        this.download(this.rabbitList[index].name);
      }
    }
  }

  download(audioName: string) {
    window.location.href = 'http://localhost/api/download/' + audioName;
  }

}
