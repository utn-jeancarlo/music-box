export class Queue {
    id: number;
    from: string;
    to: string;
    // tslint:disable-next-line:variable-name
    created_at: string;
    // tslint:disable-next-line:variable-name
    updated_at: string;
}
