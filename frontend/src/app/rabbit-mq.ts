export class RabbitMQ {
    id: number;
    from: string;
    to: string;
    name: string;
}
