
/*Create database music_box;*/
/*drop database music_box;*/

use audio;

Create table audio_store
(
  id serial primary key,
  sha256sum varchar(500),
  URL varchar(200),
  audio_extension varchar(100)
)


insert into audio_store (sha256sum,URL,audio_extesion) 
values('as2345adbfdr65434w4','localhost:258/cd/party.mp4','mp4');


use music_box;
insert into status (name) values('Pending');
insert into status (name) values('Completed');
insert into status (name) values('Failed');

insert into queue_message (queue_message.from,queue_message.to) values('temp/test.mp3','mp4');

select * from queue;

insert into audio (name,id_message,id_status) values('test',1,1);

select  * from audio;


