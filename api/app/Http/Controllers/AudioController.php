<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Audio;

class AudioController extends Controller
{
    public function index(){
        $audio = Audio::all();
        return response()->json($audio,200);
    }

    public function show($id_audio){
        $audio = Audio::all()->where('id', $id_audio)->values();
        return response()->json($audio,200);
    }

    public function store(Request $request){
        $audio = Audio::create($request->all());
        return response()->json($audio,201);
    }

    public function update(Request $request, $idAudio){
        $audio = Audio::find($idAudio);
        if($audio){
            $id_status = $request->input('id_status');
            $audio->id_status = $id_status;
            $audio->save();
            return response()->json($audio, 202);
        } else {
            return response()->json(array(
                'code' => 404,
                'message' => 'Audio not found'
            ), 404);
        }
    }
}
