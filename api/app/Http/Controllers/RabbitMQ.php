<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQ extends Controller
{
    private $connection;
    private $channel;
    private $callback_queue;
    private $response;
    private $corr_id;

    private function initializeRabbitMQ()
    {
        sleep(4);
        $this->connection = new AMQPStreamConnection('localhost',5672,'rabbitmq','rabbitmq');
        $this->channel = $this->connection->channel();
    }

    public function store(Request $request)
    {
        $this->initializeRabbitMQ();
        //$data = $request->all();
        $audio = $request->all();
         //$data = '{"id":'.$audio->id .', "from": "/audios/believer.mp3", "to": "/audios/audiosCompleted/believer.ogg"}';
        // $data = '{"id": 1, "from": "/home/jeancarlo/Git/music-box/api/audiosTemp/Bell.mp3", "to": "/home/jeancarlo/Git/music-box/api/audiosTemp/audiosCompleted/Bell.MP1"}';
        //$data = '{"id":1 ,"from": "/audios/believer.mp3", "to": "/audios/audiosCompleted/believer.ogg"}';
         $data = '{"id":'.$audio["id"]. ', "from": "'.$audio["from"] . '" , "to": "' .$audio["to"] . '" }';  
        list($this->callback_queue, ,) = $this->channel->queue_declare("",false,false,true,false);
        $this->channel->basic_consume($this->callback_queue,'',false,true,false,false,
            array(
                $this,
                'onResponse'
            )
        );

        $this->response = null;
        $this->corr_id = uniqid();
        $msg = new AMQPMessage(
            $data,
            array(
                'correlation_id' => $this->corr_id,
                'reply_to' => $this->callback_queue
            )
        );
        $this->channel->basic_publish($msg, '', 'task_queue');
        while (!$this->response) {
            $this->channel->wait();
        }
      //  echo($this->response);
         $responseRabbit = [];
         $responseRabbit["id"] = $audio["id"];
         
         if($this->response == "completed")
         {
            $responseRabbit["status"] = 2;
            $responseRabbit["newAudio"] = $audio["to"];
         }
        else
            $responseRabbit["status"] = 3;
         
         return response()->json($responseRabbit,200);
    }

    public function onResponse($rep)
    {
        if ($rep->get('correlation_id') == $this->corr_id) {
            $this->response = $rep->body;
        }
    }

    public function donwloadAduio($filename)
    {
          // Check if file exists in app/storage/file folder
        $file_path = public_path() .'/audiosTemp/audiosCompleted/'. $filename;
        if (file_exists($file_path))
        {
            // Send Download
            return Response::download($file_path, $filename, [
                'Content-Length: '. filesize($file_path)
            ]);
        }
        else
        {
            // Error
            exit($file_path);
        }
    }
   
}
