<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AudioSaveController extends Controller
{
    public function save(Request $request)
    {
      //get the audio
      $name = $request->name;
      $type = $request->type;
      $file = $request->file;

      $audio = base64_decode($file);

      $fileName = $name;
      $path = public_path();
      $url = $path.'/audiosTemp/'.$fileName;
      file_put_contents($url, $audio);

      return response()->json('Audio Converted',201);
    }
}
