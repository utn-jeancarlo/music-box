<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QueueMessage;

class QueueMessageController extends Controller
{  
    public function index($id_message){
        $queue = QueueMessage::all()->where('id', $id_message)->values();
        return response()->json($queue,200);
    }
    public function store(Request $request){
        $queue = QueueMessage::create($request->all());
        return response()->json($queue,201);
    }
}
