<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;

class StatusController extends Controller
{

    public function index($id_status){
        $status = Status::all()->where('id', $id_status)->values();
        return response()->json($status,200);
    }

    public function store(Request $request){
        $status = Status::create($request->all());
        return response()->json($status,201);
    }
}
