<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QueueMessage extends Model
{
    protected $fillable = [
        'id', 'from', 'to'
    ];
}
