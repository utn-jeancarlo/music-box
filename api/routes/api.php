<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/rabbitClient','RabbitMQ@store');

Route::get('/audio','AudioController@index');
Route::get('/audio/{id}','AudioController@show');
Route::post('/audio','AudioController@store');
Route::put('/audio/{id}','AudioController@update');

Route::get('/queue/{id}','QueueMessageController@index');
Route::post('/queue','QueueMessageController@store');

Route::get('/status/{id}','StatusController@index');
Route::post('/status','StatusController@store');

Route::post('/save','AudioSaveController@save');

Route::get('/download/{filename}','RabbitMQ@donwloadAduio')->where('filename', '[A-Za-z0-9\-\_\.]+');;