<?php
namespace Models {
    class AudioStore
    {
        private $connection;

        public function __construct($connection)
        {
            $this->connection = $connection;
        }
        public function create($audio)
        {
            $this->connection->runStatement('INSERT INTO audio_store (sha256sum,URL,audio_extension) VALUES (?,?,?)',
                                                                    [ $audio["check_sum"], $audio["URL"] , $audio["audio_extension"] ]);
        }

        public function get_audio_by_check_sum($check_sum,$audio_extension)
        {
            $sql = "select * 
                    from audio_store 
                    where sha256sum = ?  and audio_extension = ?";
            return $this->connection->runQuery($sql,[$check_sum,$audio_extension]);
        }

        public function get_all()
        {
            $sql = "select * from audio_store";
            return $this->connection->runQuery($sql);
        }
    }
}