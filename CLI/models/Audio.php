<?php
namespace Models {
    class Audio{
        private $connection;

        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function get_all()
        {
            $sql = "select * from audios";
            return $this->connection->runQuery($sql);
        }

        public function update_status($audio_id,$new_status)
        {
            $sql = 'UPDATE audios
                    set id_status = ?
                    where id_message = ?
                    ';
            $this->connection->runStatement($sql,[$new_status,$audio_id]);
        }
    }
}