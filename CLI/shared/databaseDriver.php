<?php
namespace Shared{
    require_once __DIR__ . '/../db/MySqlConnection.php';
    require_once __DIR__ . '/../models/AudioStore.php';
    require_once __DIR__ . '/../models/Audio.php';
    use db\MySqlConnection;
    use models\AudioStore;
    use models\Audio;

    class DatabaseDriver
    {
        private $audio_connection;
        private $music_box_connection;
        public  $audio_store_model;
        public  $audio_model;

        public function __construct()
        {
            $this->music_box_connection = new MySqlConnection('admin', 'password', 'music_box', 'localhost:33061');  
            $this->audio_connection     = new MySqlConnection('admin', 'password', 'audio', 'localhost:33061');   
            $this->connect_database();
            $this->setup_models();
        }

        private function connect_database()
        {
            $this->audio_connection->connect();
            $this->music_box_connection->connect();
        }
        
        private function setup_models()
        {
            $this->audio_store_model =  new AudioStore($this->audio_connection);
            $this->audio_model       =  new Audio($this->music_box_connection);

            echo(var_dump($this->audio_model->get_all()));
        }
    }
}
