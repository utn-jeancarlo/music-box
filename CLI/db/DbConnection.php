<?php

namespace Db {
    abstract class DbConnection
    {
        protected $user;
        protected $password;
        protected $database;
        protected $server;
        public function __construct($user, $password, $database, $server)
        {
            $this->user     = $user;
            $this->password = $password;
            $this->database = $database;
            $this->server   = $server;
        }

        abstract public function connect();
        abstract public function disconnect();
        abstract public function runQuery($sql, $params = []);
        abstract public function runStatement($sql, $params = []);
    }
}
