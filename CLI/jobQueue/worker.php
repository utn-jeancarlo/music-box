<?php
namespace JobQueue{

    require_once __DIR__ . '/../vendor/autoload.php';
    require_once __DIR__  .'/converter.php';
    use JobQueue\Converter;
    use PhpAmqpLib\Connection\AMQPStreamConnection;
    use PhpAmqpLib\Message\AMQPMessage;

    
    class Worker
    {
        private $connection;
        private $channel;
        private $converter;
        public function __construct($server,$port,$user,$password)
        {
            $this->converter = new Converter();
            $this->connection = new AMQPStreamConnection($server, $port, $user, $password);
        }

        //We can create one new channel when we run the service
        private function create_channel()
        {
            $this->channel = $this->connection->channel();
            $this->channel->queue_declare('task_queue', false, true, false, false);
            echo " [*] Waiting for messages. To exit press CTRL+C\n";
        }

        //Here we can close the current channel
        private function close_task($callback)
        {
            $this->channel->basic_qos(null, 1, null);
            $this->channel->basic_consume('task_queue', '', false, false, false, false, $callback);
           
        }

        //Here we are going to convert the audio to the new format
        public function run_worker_queue()
        {
            $this->create_channel();

            $callback = function ($req) {
                $n = json_decode($req->body); //Convert message  to json object
                echo  "\n".'[.] Acknowledged(', $req->body, ")\n";
                $msg = new AMQPMessage(
                    (string) $this->converter->convert_audio($n),
                    array('correlation_id' => $req->get('correlation_id'))
                );
                $req->delivery_info['channel']->basic_publish(
                    $msg,
                    '',
                    $req->get('reply_to')
                );
                $req->delivery_info['channel']->basic_ack(
                    $req->delivery_info['delivery_tag']
                );
            };



            $this->close_task($callback);
        
            while (count($this->channel->callbacks)) {
                $this->channel->wait();
            }
           
            $this->close_process();

        }

        private function close_process()
        {
            $this->channel->close();
            $this->connection->close();
        }
    }
}   