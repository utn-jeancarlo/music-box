<?php
namespace JobQueue{
    
    //echo(var_dump($audio_store_model->get_all()));
    require_once __DIR__  .'/../shared/databaseDriver.php';
    use Shared\DatabaseDriver;
    class Converter
    {
        private $database_driver;
        private $current_sha256sum;
        private $new_extension;
        private $audio_id;

        public function __construct()
        {
            $this->database_driver = new DatabaseDriver();
        }
        
         /*In this function we can convert the audio 
         by an exection on console using sox library
        */
        public function convert_audio($message)
        {
            $this->set_new_extension($message->to);
            $this->audio_id = $message->id;
            if(!$this->verify_sha256sum_exists($message->from))
            {
                echo("\n[/*/]Creating a new one...");
                $command = "sox ". $message->from . " ".$message->to;
                exec( $command, $outputCommand, $status);
                if($status == 2)
                {
                    $this->update_status(3); // 3 = failed
                    echo("\n[*]Failed\n");
                    return "failed";
                }
                $this->add_new_audio_on_audio_store($message->to);
            }
            $this->update_status(2); // 2 = Completed
            echo("\n[*]Completed\n");
            return "completed";
        }  
        

        /*
        Get the extesion for the new audio
        */
        private function set_new_extension($new_sound_path)
        {
            preg_match('/\.(.+)/', $new_sound_path, $output_array);
            echo(" ".$output_array[1]. " is the new extesion...  \n");
            $this->new_extension = $output_array[1];
        }
        
        /*
        Here we are going to verify if the audio 
        with new format exists in our database
        */
        private function verify_sha256sum_exists($sound_path)
        {   
            $command = "sha256sum ".$sound_path;
            exec( $command, $outputCommand, $status);

            $input_line = (string) $outputCommand[0];
            preg_match('/(\w.+?)\s/', $input_line, $output_array);
            $this->current_sha256sum = $output_array[1];

            $arraysha256sum = $this->database_driver->audio_store_model->get_audio_by_check_sum($this->current_sha256sum,$this->new_extension);
            return count($arraysha256sum) > 0 ? true : false;
        }

        /*
        In this function we can add the new sha256sum , url and new extesion on database
        */
        private function add_new_audio_on_audio_store($new_path)
        {   
            $new_audio = [];
            $new_audio["check_sum"] = $this->current_sha256sum;
            $new_audio["URL"] = $new_path;
            $new_audio["audio_extension"] = $this->new_extension;
            $this->database_driver->audio_store_model->create($new_audio);
            echo(" *** it was saved  in the database **** \n");
        }

        /*
         Here we can update the audio status on the database
        */
        private function update_status($status)
        {
            $this->database_driver->audio_model->update_status($this->audio_id,$status);
        }
    }
}